

	window.rnwWidget = window.rnwWidget || {};
	window.rnwWidget.configureWidget = function(options) 
	{
		options.widget.on(window.rnwWidget.constants.events.PAYMENT_METHOD_CHANGED, function(event) 
		{
            //console.log('change');
            //event.widget.showStep("customer-address");
		});
		
		options.extend(
		{
			custom_fields: 
			{
				stored_customer_companydonationnumber:
				{
					type: 'text',
					location: 'after',
					reference: 'customer_birthdate',
					label: 'Spendennummer',
					placeholder: 'Spendennummer (optional)'
				},
				
				stored_customer_companyphone:
				{
					type: 'text',
					location: 'after',
					reference: 'customer_birthdate',
					label: 'Telefon',
					placeholder: 'Telefon (optional)'
				},
				
				stored_customer_companyname:
				{
					type: 'text',
					location: 'after',
					reference: 'customer_birthdate',
					label: 'Unternehmen',
					placeholder: 'Unternehmen  (optional)'
				},
				
				stored_customer_isCompany:
				{
					type: 'checkbox',
					location: 'after',
					reference: 'customer_birthdate',
					label: 'Spende als Unternehmen',
					value: 'true',
				},
			}
		});
		
		options.epikOptions.test_mode = "false";
		options.defaults['stored_toys_ordered'] = '';
		if(is_toyshop != 1)
		{
			
			if(donation_amount != "")
				options.defaults['ui_onetime_amount_default'] = donation_amount * 100;	
		}
		else
			options.defaults['ui_onetime_amount_default'] = '0';
		
	    options.widget.on(window.rnwWidget.constants.events.WIDGET_LOADED, function(event) 
		{
	    	$('.lema-overlay-trigger[data-target="data-protection"]').click(function()
	    	{
	    		setTimeout(function()
	    		{
	    			$('.lema-overlay-bg').css({'z-index': 99});
	    		}, 100);
	    	});
	    	
	    	
	    	event.widget.showBlock("customer_birthdate");
	    	event.widget.hideStep("donation-target");
	        if(is_toyshop == 1)
	        {
	        	event.widget.hideStep("amount");
	        }
	        else
	        {
	        	//$('.lema-step-row[data-block-id="recurring_interval_selector"]').hide();
	        }
	    });
	};

	
	$(document).ready(function()
	{	
		if(is_toyshop == 1)
		{
			toggleToyshopListeners(true);
		}
		else
		{
				
		}
	});	
	
	function toggleToyshopListeners(toggle)
	{
		if(toggle)
		{
			$(".toy_input, .toy_extra_amount").keydown(function (e) 
			{
		        // Allow: backspace, delete, tab, escape, enter and .
		        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		             // Allow: Ctrl+A, Command+A
		            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
		             // Allow: home, end, left, right, down, up
		            (e.keyCode >= 35 && e.keyCode <= 40)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) 
		        {
		            e.preventDefault();
		        }
		    });
			
			
			$(".toy_input, .toy_extra_amount").keyup(function()
			{
				if($(this).val() == '')
					$(this).val('0');
				
				calcToyTotal();
			});
		}
		else
		{
				
		}
	}
	
	
	function calcToyTotal()
	{
		var toy_total = 0;
		var toy_price = 12;
		var extra = parseInt($('.toy_extra_amount').val());
		var toy_text = "";
		
		$(".toy_input").each(function()
		{
			toy_total += parseInt($(this).val());
			toy_text += parseInt($(this).val()) + ' ' + $(this).attr('toy_name') + ', ';
		});
		
		
		var total = ((toy_total * toy_price) + extra) * 100;
		
		$('input[type="hidden"][name="amount"]').val(total);
		$('#donation_total').empty().html(total/100);
		
		$('input[type="hidden"][name="stored_toys_ordered"]').val(toy_text);
	}