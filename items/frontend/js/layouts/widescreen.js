
function widescreen()
{
	submenu_pos = 110;
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// FUNCTIONALITY
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// CSS
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	$('.containerwidth').css(
	{
		'width': '770px',
		'margin-left': (wWidth-800)/2,
		'margin-right': (wWidth-800)/2,
	});
	
	$('#menu_container').css(
	{
		'padding-left': '15px',
		'padding-right': '15px'
	});
	
	$('#menu_container li').css(
	{
		'margin-right': 40
	});
	
	$('#menu_container li.menu_item_search').css(
	{
		'margin-right': 0
	});
			
	
	$('.containerborder').css(
	{
		'border-left': 'solid 15px #ffffff',
		'border-right': 'solid 15px #ffffff',
	});
	
	$('#menu_upper_container').css(
	{
		'width': '800px',
		'margin-left': (wWidth-800)/2,
		'margin-right': (wWidth-800)/2,
	});
	
	$('#menu_logo').css(
	{
		'left': (wWidth/2) - 420 - 220,
	});	
	
	$('#footer').css(
	{
		'display': 'block'
	});
	
	$('#footer_container').css(
	{
		'margin-left': (wWidth-800)/2,
		'margin-right': (wWidth-800)/2,
		'width': 800,
		'padding-left': 0
	});
	
	$('.footer_column').css(
	{
		'width': 380,
		'margin-right': 20,
	});
	
	$('#footer_social, #footer_donations, #footer_menu').css(
	{
		'width': 800,
		'padding-left': 0
	});
	
	$('#headline_container, .headline_video, .headline_img').css(
	{
		'width': 770,
		'height': 325,
	});
	
	$('.headline').css(
	{
		'width': 740,
		'height': 295
	});
	
	$('#articles').css(
	{
		'width': 770,
		'padding-left': 0,
		'padding-right': 0
	});
	
	$('.upper_menu_right').css(
	{
		'float': 'right',
		'margin-left': 10,
		'margin-right': 0
	});
	
	$('.upper_menu_social').css(
	{
		'height': 30
	});	

	$('.upper_menu_left').css(
	{
		'float': 'left',
		'height': 30
	});
	
	$('#menu_upper_container_social').css(
	{
		'float': 'right',
		'margin-top': 0
	});
	
	$('.upper_menu_newsletter').css(
	{
		'display': 'block',
		'line-height': '18px',
		'padding': '4px 35px',		
	});
	
	$('#menu_upper').css(
	{
		'height': 60
	});
	
	$('#menu_shadow_bottom').css(
	{
		'top': 60
	});
	
	$('#menu').css(
	{
		'top': 60
	});	
	
	$('#container').css(
	{
		'margin-top': 110
	});	

	$('.headline_text').css(
	{
		'font-size': 30
	});	
	
	$('#donate_overlay').css(
	{
		'top': 220,
		'display': 'block'
	});
	
	$('.submenu').css(
	{
		'top': 110
	});
	
	$('#article').css(
	{
		'width': 800,
		'margin-left': (wWidth-800)/2,
		'margin-right': (wWidth-800)/2,
		'margin-top': 110
	});	

	$('#article_content').css(
	{
		'width': 540
	});
	
	$('#article_sidebar').css(
	{
		'width': 245,
		'display': 'block'
	});	
	
	$('.article_preview').css(
	{
		'width': 750,
	});	
	
	$('.article_preview_text').css(
	{
		'width': 450,
		'margin-top': 0,
	});	
	
	$('#search_overlay').css(
	{
		'right': (wWidth-800)/2 + 15,
	});
}