var wWidth;
var wHeight;

$(document).ready(function()
{	
	resize();
	
	setTimeout(function(){
		resize();
	}, 2500);
	
});

$(window).resize(function()
{
	resize();
});

function resize()
{
	wWidth = $(window).width();
	wHeight = $(window).height();
	
	
	position_logo();
	arrange_bg();
	
	if(wWidth >= 800)
		widescreen();
	else if(wWidth < 800 && wWidth >= 450)
		smallscreen();
	else
		mobiledevice();
}


function position_logo()
{
	if(wWidth <= 800)
	{
		$('#menu_logo').fadeOut(150);
		$('.logo_small').fadeIn(150);
	}
	else
	{
		$('#menu_logo').fadeIn(150);
		$('.logo_small').fadeOut(150);		
	}
}


function arrange_bg()
{
	var iWidth;
	var iHeight;
	var ratio;
	var new_height;
	var new_width;
	var left;
	var top;
	

	var ratio = 1.5;

	if(wWidth > wHeight)
	{
		new_height = wHeight;
		new_width = parseInt(wHeight * ratio);
		if(new_width < wWidth)
		{
			new_width = wWidth;
			new_height = parseInt(wWidth / ratio);
			left = 0;
			top = (parseInt((new_height-wHeight) /-2));
		}
		else
		{
			top = 0;
			left = parseInt((new_width - wWidth) / -2);		
		}
	}
	else
	{
		new_width = wWidth;
		new_height = parseInt(wWidth / ratio);
		if(new_height < wHeight)
		{
			new_height = wHeight;
			new_width = parseInt(wHeight * ratio);
			top = 0;
			left = parseInt((new_width - wWidth) / -2);
		}
		else
		{
			left = 0;
			top = parseInt((new_height - wHeight) / -2);	
		}
	}

	$('#bg_image').css({'width' : new_width, 'height': new_height, 'left': left, 'top': top});

}