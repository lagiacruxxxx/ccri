var submenu_animation_duration = 300;
var donateoverlay_animation_duration = 300;
var submenu_pos_open = 0;
var submenu_pos_close = 0;
var scrolled = false;

$(document).ready(function()
{	
	ignitePackery();
	toggleMenuListeners(true);
	toggleDonateOverlayListeners(true);
	igniteQTIP();
	toggleNewsletterListeners(true);
	toggleSearchListeners(true);
	toggleSharingListeners(true);
	MobileMenu();
});

/*************************************************************************************************************************
	WINDOW SCROLL FOR SAFARI FOOTER FIX
**************************************************************************************************************************/
	
$( window ).scroll(function() {
	
		$('#footer').css({'display': 'block'});
		
		
	
});




/*************************************************************************************************************************
	PACKERY
**************************************************************************************************************************/
function ignitePackery()
{
	$('#articles').packery(
	{
		itemSelector: '.article',
		gutter: 10,
	});
}


/*************************************************************************************************************************
	QTIP
**************************************************************************************************************************/
function igniteQTIP()
{
	$('.donate_overlay_button img').qtip(
	{
		position: 
		{
			my: 'right center',
			at: 'left center,',
			adjust:
			{
				x: -10
			}
		},
	});
}

/*************************************************************************************************************************
	DONATE OVERLAY
**************************************************************************************************************************/
function toggleDonateOverlayListeners(toggle)
{
	if(toggle)
	{
		$('#donate_overlay_preview').click(function()
		{
			toggleDonateOverlay();
		});
		
		$('#donate_overlay_donation_button').click(function()
		{
			var donation_type = $('.donate_overlay_donation_box input[type=radio]:checked').val();
			var donation_amount = parseInt($('.donate_overlay_donation_box input[type=text]').val());
			console.log(donation_type + ' ' + donation_amount);
			window.location.href = rootUrl + 'subsite/' + prettyurl_donate + '?donation_type=' + donation_type + '&donation_amount=' + donation_amount;
		});
		
		$('#donate_overlay_donation_button2').click(function()
		{
			var donation_type = $('.donate_overlay_donation_box2 input[type=radio]:checked').val();
			var donation_amount = parseInt($('.donate_overlay_donation_box2 input[type=text]').val());
			console.log(donation_type + ' ' + donation_amount);
			window.location.href = rootUrl + 'subsite/' + prettyurl_donate + '?donation_type=' + donation_type + '&donation_amount=' + donation_amount;
		});
	}
	else
	{
		$('#donate_overlay_preview').unbind('click');
		$('#donate_overlay_donation_button').unbind('click');		
		$('#donate_overlay_donation_button2').unbind('click');
	}
}

function toggleDonateOverlay()
{
	toggleDonateOverlayListeners(false);
	if($('#donate_overlay').hasClass('open'))
	{
		$('#donate_overlay').animate({'width': 60}, donateoverlay_animation_duration, function()
		{
			$('#donate_overlay').removeClass('open');
			toggleDonateOverlayListeners(true);
		});
	}
	else
	{
		$('#donate_overlay').animate({'width': 325}, donateoverlay_animation_duration, function()
		{
			$('#donate_overlay').addClass('open');
			toggleDonateOverlayListeners(true);
		});
	}
}

/****************************************************************************************************************************
	MENU
**************************************************************************************************************************/
function toggleMenuListeners(toggle)
{
	if(toggle)
	{
		$('.menu_item').click(function()
		{
			toggleSubmenu($(this).attr('target'));
		});
		
	}
	else
	{
		$('.menu_item').unbind('click');
	}
}


function toggleSubmenu(target)
{
	toggleMenuListeners(false);
	
	if($('.submenu.open').length > 0)
	{
		if($('.submenu.open').attr('target') != target)
		{
			$('.submenu.open').animate({'height': 0, 'padding-top': 0, 'padding-bottom': 0, 'max-height': 0}, submenu_animation_duration, function()
			{
				$(this).removeClass('open');
			
				$('.submenu[target="' + target + '"]').css('height', 'auto');
				$('.submenu[target="' + target + '"]').animate({'max-height': 1000, 'padding-top': 15, 'padding-bottom': 15}, submenu_animation_duration, function()
				{
					$(this).addClass('open');
					toggleMenuListeners(true);
				});
			});
		}
		else
		{
			$('.submenu.open').animate({'height': 0, 'padding-top': 0, 'padding-bottom': 0, 'max-height': 0}, submenu_animation_duration, function()
			{
				$(this).removeClass('open');
				toggleMenuListeners(true);
			});
		}
	}
	else
	{
		$('.submenu[target="' + target + '"]').css('height', 'auto');
		$('.submenu[target="' + target + '"]').animate({'max-height': 1000, 'padding-top': 15, 'padding-bottom': 15}, submenu_animation_duration, function()
		{
			$(this).addClass('open');
			toggleMenuListeners(true);
		});
	}
}


/****************************************************************************************************************************
	NEWSLETTER
**************************************************************************************************************************/
function toggleNewsletterListeners(toggle)
{
	if(toggle)
	{
		$('#footer_newsletter_register, #contact_newsletter_register').click(function()
		{
			save_newsletter_email($(this));
		});
		
		$('#cookie_warning_button').on('click', function()
		{
			docCookies.setItem('cookie_accepted', 1);
			$('#cookie_warning').remove();
		});
	}
	else
	{
		$('#footer_newsletter_register').unbind('click');
		$('#contact_newsletter_register').unbind('click');
		$('#cookie_warning_button').off('click');
	}
}

function save_newsletter_email(register)
{
	if(register.attr('id') == 'footer_newsletter_register')
	{
		var email = $('#footer_newsletter').val(); 
		var input = $('#footer_newsletter');
		var success = $('#footer_newsletter_success');
	}
	else
	{
		var email = $('#contact_newsletter').val(); 
		var input = $('#contact_newsletter');
		var success = $('#contact_newsletter_success');
	}
	
	$.ajax(
	{
        url: rootUrl + 'St_anna/save_newsletter_email',
        data: {email: email},
        type:"POST",
        cache: false,
        success: function(data) 
        {
        	register.fadeOut(200);
        	input.fadeOut(200, function()
        	{
            	success.fadeIn(200);
        	});
        }
    });		
}



/****************************************************************************************************************************
	SEARCH
**************************************************************************************************************************/
function toggleSearchListeners(toggle)
{
	if(toggle)
	{
		$('.menu_item_search').click(function()
		{
			if($('.submenu.open').length > 0)
			{
				toggleSubmenu($('.submenu.open').attr('target'));
			}
			
			var elem = $('#search_overlay');
			toggleSearchListeners(false);
			if(elem.hasClass('open'))
			{
				elem.animate({'top': '-=30'}, 100, function()
				{
					elem.removeClass('open');
					toggleSearchListeners(true);
				});
			}
			else
			{
				elem.animate({'top': '+=30'}, 100, function()
				{
					elem.addClass('open');
					elem.find('input').focus();
					toggleSearchListeners(true);
				});
			}
		});
		
		$('#search_input').keyup(function(e)
		{
			if (e.keyCode == 13)
			{
				window.location.href = rootUrl + 'search?param=' + encodeURIComponent($('#search_input').val());
			}
		});
	}
	else
	{
		$('.menu_item_search').unbind('click');
		$('#search_input').unbind('keyup');
	}
}


/****************************************************************************************************************************
SHARING
**************************************************************************************************************************/
function toggleSharingListeners(toggle)
{
	if(toggle)
	{
		$('.footer_social_item').on('click', function()
		{
			switch($(this).attr('share'))
			{
				case 'facebook':
					shareFacebook();
					break;
				case 'twitter':
					shareTwitter();
					break;
				case 'mail':
					shareMail();
					break;
			}
		});
	}
	else
	{
		$('.footer_social_item').off('click');
	}
}


function shareFacebook()
{
	FB.ui(
	{
		method: 'share',
		href: window.location.href,
	}, function(response){
	});
}

function shareTwitter()
{
	window.open(
	  'https://twitter.com/share?url='+encodeURIComponent(window.location.href) + '&hashtags=CCRI',
      'facebook_share_dialog', 
      'width=626,height=436'); 
}

function shareMail()
{
	var link = "mailto:" + "&body=" + encodeURIComponent(window.location.href);
	window.location.href = link;
}

/****************************************************************************************************************************
MOBILE MENU
**************************************************************************************************************************/
function MobileMenu()
{
$('#mobile_menu_button').click(function()
{
	if(!$(this).hasClass('open'))
	{
		window.scrollTo(0,0);
		$(this).addClass('open');
		$('.mobile_submenu').stop().animate({top:-1500},500)
		$('#mobileMenu').stop().animate({top:150},500);
	}
	else
	{
		
		$(this).removeClass('open');
		$('#mobileMenu').stop().animate({top:-1500},1000);
	}
	unbindMobile();
	MobileMenu();
});


$('.mobile_menu_item').click(function(){
	var target = $(this).attr('target');
	
	$('#mobile_menu_button').removeClass('open');
	$('#mobileMenu').stop().animate({top:-1500},500, function()
	{
		window.scrollTo(0,0);
		$('.mobile_submenu[target="'+target+'"]').stop().animate({top: 150},500);
	});
});
}

function unbindMobile()
{
$('#mobile_menu_button').unbind('click');
}
