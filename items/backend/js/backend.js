var areaselect = null;

$(document).ready(function()
{	
	resize();
	addGlobalListeners();

	switch(active_site)
	{
		case 'backgrounds':
			igniteBackgroundSortable();
			toggleBackgroundOrderingListeners(true);
			break;
			
		case 'frontpage_articles':
			igniteFrontpageArticlesSortable();
			toggleFrontpageArticlesListeners(true);
			break;
			
		case '':
		default:
			break;
		

	}
	
});

function addGlobalListeners()
{
	$(window).resize(function()
	{
		resize();
	});
}

function resize()
{
	var wWidth = $(window).width();
	var wHeight = $(window).height();
	
	$('#menu').css({'width' : wWidth});
	$('#sidebar').css({'height': wHeight - $('#menu').height()});
	$('#content').css({'height': wHeight - $('#menu').height() -40, 'width': wWidth - $('#sidebar').width() -40});
}


/****************************************************************************************************************************************************************************
	BACKGROUND ORDERING
******************************************************************************************************************************************************************************/
function igniteBackgroundSortable()
{
	$('#backgrounds_active ul, #backgrounds_available ul').sortable(
	{
		connectWith: '.backgrounds_sortable',
		receive: function(event, ui)
		{
			if(ui.sender.hasClass('backgrounds_active_container'))
			{
				ui.item.unbind('mouseenter mouseleave');
				ui.item.find('.background_remove').fadeOut(0);
			}
			else
			{
				toggleBackgroundOrderingListeners(false);
				toggleBackgroundOrderingListeners(true);
			}
			checkAvailableBackgrounds();

		},
	});
	$('#backgrounds_active ul, #backgrounds_available ul').disableSelection();
	checkAvailableBackgrounds();
}

function checkAvailableBackgrounds()
{
	if($('.backgrounds_available_container .backgrounds').length > 0)
	{
		$('.no_articles_available').fadeOut(0);
		$('#backgrounds_available ul').sortable( "option", "disabled", false );
	}
	else
	{
		$('.no_articles_available').fadeIn(0);
		$('#backgrounds_available ul').sortable( "option", "disabled", true );
	}
}

function toggleBackgroundOrderingListeners(toggle)
{
	if(toggle)
	{
		$('#backgrounds_active li').hover(function()
		{
			$(this).find('.background_remove').fadeIn(50);
		}, 
		function()
		{
			$(this).find('.background_remove').fadeOut(50);
		});
		
		$('.background_remove').click(function()
		{
			resetBackground($(this).parent());
		});	
		
		$('.backgrounds_save').click(function()
		{
			saveBackgrounds();
		});
	}
	else
	{
		$('#backgrounds_active li').unbind('mouseenter mouseleave');
		$('.background_remove').unbind('click');
		$('.backgrounds_save').unbind('click');
	}
}

function resetBackground(background)
{
	background.unbind('mouseenter mouseleave');
	background.find('.background_remove').fadeOut(0);
	background.appendTo('#backgrounds_available ul');
	checkAvailableBackgrounds();
}

function saveBackgrounds()
{
	var i = 0;
	var backgrounds = [];
	$('#backgrounds_active li').each(function()
	{
		backgrounds.push
		( 
			{
				'background_id': $(this).attr('background_id'),
				'ordering': i
			}
		);
		i++;
	});
	
	$.ajax({
        url: rootUrl + 'backend/save_background_ordering',
        data: JSON.stringify(backgrounds),
        contentType: "application/json; charset=utf-8",
        type:"POST",
        dataType: "json",
        cache: false,
        success: function(data) 
        {
        	if(data.success)
				alert(data.message);
			else
				alert(data.message);
        }
    });			
}



/****************************************************************************************************************************************************************************
	FRONTPAGE ARTICLES
******************************************************************************************************************************************************************************/
function igniteFrontpageArticlesSortable()
{
	$('#articles_active_container ul, #articles_available ul').sortable(
	{
		connectWith: '.articles_sortable',
		remove: function(event, ui)
		{
			checkAvailableArticles();
		}
	});
	$('#articles_active_container ul, #articles_available ul').disableSelection();
	checkAvailableArticles();
}

function checkAvailableArticles()
{
	if($('#articles_available .article_available').length > 0)
	{
		$('.no_articles_available').fadeOut(0);
		$('#articles_available ul').sortable( "option", "disabled", false );
	}
	else
	{
		$('.no_articles_available').fadeIn(0);
		$('#articles_available ul').sortable( "option", "disabled", true );
	}	
}

function toggleFrontpageArticlesListeners(toggle)
{
	if(toggle)
	{
		$('.article_total').hover(function()
		{
			if($(this).parent().parent().attr('id') != 'articles_available')
				$(this).find('.article_remove').fadeIn(0);
		}, 
		function()
		{
			if($(this).parent().parent().attr('id') != 'articles_available')
				$(this).find('.article_remove').fadeOut(0);
		});
		
		$('.article_remove').click(function()
		{
			resetArticle($(this).parent());
		});	
		
		$('.frontpage_articles_save').click(function()
		{
			saveFrontpageArticles();
		});
	}
	else
	{
		$('.article_total').unbind('mouseenter mouseleave');
		$('.article_remove').unbind('click');
		$('.frontpage_articles_save').unbind('click');
	}

}

function resetArticle(article)
{
	article.find('.article_remove').fadeOut(0);
	article.find('.article_available').fadeOut(0);
	article.appendTo('.articles_sortable');
	article.remove();
	checkAvailableArticles();
}


function saveFrontpageArticles()
{
	var i = 0;
	var articles = [];
	$('#articles_active_container li').each(function()
	{
		articles.push
		( 
			{
				'article_id': $(this).attr('article_id'),
				'ordering': i
			}
		);
		i++;
	});
	
	$.ajax({
        url: rootUrl + 'backend/save_frontpage_articles',
        data: JSON.stringify(articles),
        contentType: "application/json; charset=utf-8",
        type:"POST",
        dataType: "json",
        cache: false,
        success: function(data) 
        {
        	if(data.success)
				alert(data.message);
			else
				alert(data.message);
        }
    });		
}


function blur_image(filename, uploadpath, element, imagedata)
{
	setTimeout(function()
	{
		$('#' + element).parent().find('.bc_col_image_preview').attr('src', rootUrl + 'items/backend/img/blur_preview.png');
	}, 200);
			
	$.ajax(
	{
        url: 'http://54.217.247.147/st_anna_blur.php',
        data: { filename: filename, data: imagedata },
		method: 'POST',
		dataType: 'json',
        success: function(data) 
        {
        	console.log(data)
        	$.ajax(
			{
		        url: rootUrl + 'backend/save_blurred_image',
		        data: { fname: data , old_filename: filename, uploadpath: uploadpath},
				method: 'POST',
				dataType: 'json',
		        success: function(data) 
		        {
		        	var d = new Date();
		        	$('#' + element).parent().find('.bc_col_image_preview').attr('src', rootUrl + '/' + uploadpath + '/' + filename + '?' + d.getTime());
		        }
		    });	
        }
    });	
}


function cropUpload(filename, uploadpath, element, result)
{
	var html = '<div id="upload_crop"><img id="upload_crop_img" src="' + rootUrl + uploadpath + filename + '" /><div id="upload_crop_btn">CROP</div></div><div id="upload_crop_fade"></div>';
	$('body').append(html);
	
	$('#upload_crop_fade').on('click', function()
	{
		$('#upload_crop_fade').remove();
		$('#upload_crop').remove();
	});
	
	var wWidth = $(document).width();
	var wHeight = $(document).height();
	var padding = 30 * 2;
	
	imagesLoaded($('#upload_crop'), function()
	{
		var iWidth = $('#upload_crop_img').get(0).naturalWidth;
		var iHeight = $('#upload_crop_img').get(0).naturalHeight;
		var ratio = iWidth / iHeight;
		var cWidth = iWidth + padding;
		var cHeight = iHeight + padding + $('#upload_crop_btn').height();
		
		if(cWidth > wWidth * 0.8)
		{
			iWidth = wWidth * 0.8 - padding;
			iHeight = iWidth / ratio;
			cWidth = iWidth + padding;
			cHeight = iHeight + padding;
		}
		
		if(cHeight > wHeight * 0.8)
		{
			iHeight = wHeight * 0.8 - padding - $('#upload_crop_btn').height();
			iWidth = iHeight * ratio;
			cWidth = iWidth + padding;
			cHeight = iHeight + padding;			
		}
		
		$('#upload_crop').css({'left': (wWidth - cWidth)/2, 'top': (wHeight - cHeight)/2});

		$('#upload_crop_img').animate({'width': iWidth, 'height': iHeight}, 250, function()
		{
			areaselect = $('#upload_crop_img').imgAreaSelect(
			{ 
				aspectRatio: '250:144', 
				handles: true,
				x1: 0,
				y1: 0,
				x2: 250,
				y2: 144,
				parent: '#upload_crop',
				instance: true,
			});
		});
	});		

	$('#upload_crop_btn').on('click', function()
	{
		$.ajax(
		{
			url: rootUrl + 'backend/crop_upload_img',
			data: { filename: filename, uploadpath: uploadpath, x1: areaselect.getSelection().x1, y1: areaselect.getSelection().y1, x2: areaselect.getSelection().x2, y2: areaselect.getSelection().y2},
			method: 'POST',
			cache: false,
			dataType: 'json',
			success: function(data)
			{
				var ret = data;
				
				if(ret.success)
				{
					$('#upload_crop_fade').click();
					$('#' + element).parent().find('.bc_col_image_preview').attr('src', $('#' + element).parent().find('.bc_col_image_preview').attr('src').split("?")[0] + "?" + new Date().getTime());
				}
				else
				{
					alert('Error while cropping/masking/blurring');
				}
			}
		});
	});
}
