

function new_image_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		img: null,
		type: 'image',
		uploadpath: 'items/general/uploads/content_img',
		alttext: '',
		fname: '',
		margin_top: 0,
		margin_bottom: 0,
		align: 2,
		width: 0,
		
		
		// MODULE BASIC
		init: function(parent)
		{
			var html = '<div class="module module_image" module_id="' + this.id + '"><img src="' + rootUrl + 'items/backend/img/image_upload_placeholder.png' + '" /></div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			this.img = '.module[module_id="' + this.id + '"] img';
			this.bindListeners();
		},
		
		bindListeners: function()
		{
			$(this.selector).click(function()
			{
				modules[$(this).attr('module_id')].setActive();
			});
			
		},
		
		unbindListeners: function()
		{
			$(this.selector).unbind('click');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
			html += '<tr> <td class="module_properties_label"><label>Alt text</label></td> <td class="module_properties_input"><input type="text" method="setAlttext" value="' + this.alttext + '"></td> </tr>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Top</label></td> <td class="module_properties_input"><input type="text" method="setMarginTop" value="' + this.margin_top + '"></td> </tr>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Bottom</label></td> <td class="module_properties_input"><input type="text" method="setMarginBottom" value="' + this.margin_bottom + '"></td> </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="upload">Upload</div> <input type="file" id="module_properties_upload_button" accept=".png,.jpg,.jpeg"/> </td> </tr>';
			html += '</table>';
			
			parent.html(html);
			this.uploadListeners();
			togglePropertyListeners(true);
		},
		
		activate: function()
		{
			this.setActive();
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.hideOptions();
			}
		},

		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			this.showProperty(module_properties);
			this.showOptions();
		},
		
		showOptions: function()
		{
			var html = '<div class="module_options">';
			html += module_move_option();
			html += module_remove_option();
			html += '</div>';
			$(this.selector).append(html);
			toggleOptionListeners(true);
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		
		// PROPERTY SETTERS/GETTERS
		setMarginTop: function(margin_top)
		{
			this.margin_top = parseInt(margin_top);
			$(this.selector).css({'margin-top': this.margin_top});
		},
		
		setMarginBottom: function(margin_bottom)
		{
			this.margin_bottom = parseInt(margin_bottom);
			$(this.selector).css({'margin-bottom': this.margin_bottom});
		},
		
		setAlttext: function(alttext)
		{
			this.alttext = alttext;
			$(this.selector).attr('title', alttext);
		},
		
		
		getSaveData: function()
		{
			var ret = 
			{
				'fname': this.fname,
				'margin_top': this.margin_top,
				'margin_bottom': this.margin_bottom,
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,
				'width': this.width,
				'alttext': this.alttext,
			};
			
			return ret;
		},
		
		load: function(properties)
		{		
			this.setImage(properties);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setAlttext(properties.alttext);
		},
		
		
		
		//MODULE SPECIFIC
		upload: function()
		{
			$('.module_properties_button[method="upload"]').parent().find('input').click();
		},
		
		uploadListeners: function()
		{
			$('#module_properties_upload_button').change(function()
			{
				var element = $(this).attr('id');
				var file = document.getElementById(element).files[0];
				var uploadpath = active_module.uploadpath;
				var reader = new FileReader();
				var url;
				reader.readAsDataURL (file);
				reader.onload = function(event)
				{
					var result = event.target.result;
					$.ajax(
					{
						url: rootUrl + 'backend/content_imageupload',
						data: { filename: file.name, data: result , uploadpath: uploadpath},
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							if(ret.success)
							{
								$(active_module.img).attr('src', rootUrl + uploadpath + '/' + ret.filename);
								active_module.fname = ret.filename;
								imagesLoaded(active_module.img, function()
								{
									console.log('images_loaded');
									active_module.igniteResize(true);
									active_module.setImageWidth($(active_module.img).width());
								});
							}
							else
							{
								alert('Error while uploading');
							}
						}
					});			
				};
			});
		},
		
		setImage: function(properties)
		{
			$(active_module.img).attr('src', rootUrl + this.uploadpath + '/' + properties.fname);
			$(active_module.img).css({width: properties.width, 'margin-left': 'auto', 'margin-right': 'auto'});
			this.fname = properties.fname;
			this.igniteResize(false);
			this.setImageWidth(properties.width);
			
			//$(active_module.img).parent().css({width: properties.width, 'margin-left': 'auto', 'margin-right': 'auto'});
		},
		
		setImageWidth: function(width)
		{
			this.width = width;
			$(this.img).parent().width(width);
			$(this.img).width(width);
		},
				
		igniteResize: function(isnew)
		{
			if(isnew)
			{
				$(active_module.img).resizable(
				{
					aspectRatio: true,
					ghost: true,
					maxWidth: 510,
					handles: 's',
					
					start: function(event, ui)
					{
						modules[ui.element.parent().attr('module_id')].activate();
					},
			      
			        stop: function( event, ui ) 
			        {
				      	$(active_module.img).parent().css({'margin-left': 'auto', 'margin-right': 'auto'});
				      	$(active_module.img).css({'margin-left': 'auto', 'margin-right': 'auto'});
				      	active_module.setImageWidth($(active_module.img).width());
				    },
			        create: function( event, ui ) 
			        {
			        	$(active_module.img).css({width:'auto', height: 'auto','margin-left': 'auto', 'margin-right': 'auto'}); 
				    	$(active_module.img).parent().css({width:'auto', height: 'auto','margin-left': 'auto', 'margin-right': 'auto'});
			        }
			    });
			}
			else
			{
				$(active_module.img).resizable(
				{
					aspectRatio: true,
					ghost: true,
					maxWidth: 510,
					handles: 's',
					
					start: function(event, ui)
					{
						modules[ui.element.parent().attr('module_id')].activate();
					},
			      
			        stop: function( event, ui ) 
			        {
			          	$(active_module.img).parent().css({'margin-left': 'auto', 'margin-right': 'auto'});
		      	      	$(active_module.img).css({'margin-left': 'auto', 'margin-right': 'auto'});
		      	      	active_module.setImageWidth($(active_module.img).width());
				    },
			        create: function( event, ui ) 
			        {
			        	$(active_module.img).css({height: 'auto','margin-left': 'auto', 'margin-right': 'auto'}); 
				    	$(active_module.img).parent().css({ height: 'auto','margin-left': 'auto', 'margin-right': 'auto'});
			        }
			    });
			}
		},
	}
	
	return new_elem;
}