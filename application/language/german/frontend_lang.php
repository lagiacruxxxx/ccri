<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***********************************************************************************************
 * VARIABLES
 ***********************************************************************************************/


/***********************************************************************************************
 * STRINGS
 ***********************************************************************************************/
$lang['newsletter'] = "Newsletter";
 
$lang['menu_home'] = "Home";
$lang['menu_research'] = "Forschung";
$lang['menu_news'] = "News";
$lang['menu_donate'] = "Spenden";
$lang['menu_about'] = "&Uuml;ber uns";
$lang['menu_contact'] = "Kontakt";
$lang['menu_science'] = "Science";
 
$lang['donate_overlay_preview'] = "JETZT SPENDEN!";
$lang['donate_overlay_header'] = "Ihre Spende hilft!";
$lang['donate_overlay_count_header'] = "ICH HELFE";
$lang['donate_overlay_count_once'] = "einmalig";
$lang['donate_overlay_count_continued'] = "regelm��ig";
$lang['donate_overlay_amount_header'] = "MEINE SPENDE";
$lang['donate_overlay_amount_currency'] = "MEINE SPENDE";
$lang['donate_overlay_button'] = "JETZT SPENDEN";

$lang['newsarticle_readmore'] = "Mehr erfahren";

$lang['footer_header_social'] = "Folgen sie uns";
$lang['footer_header_newsletter'] = "Newsletter abonnieren";
$lang['footer_header_donateaccount'] = "Spendenkonto";
$lang['footer_header_donateservice'] = "Spendenservice";
$lang['footer_donatenow'] = "Jetzt spenden";
$lang['footer_menu_sitemap'] = "Sitemap";
$lang['footer_menu_impress'] = "Impressum";
$lang['footer_menu_contact'] = "Kontakt";
$lang['footer_menu_privacy'] = "Datenschutzbestimmungen";
$lang['footer_copyright_header'] = "Copyright";
$lang['footer_copyright_text'] = "&copy; 2015 St. Anna Kinderkrebsforschung";

$lang['donation_sum_header'] = "Bitte geben sie die H&ouml;he der gew&uuml;nschten Spende an:";
$lang['donation_payment_header'] = "Zahlungsart:";
$lang['donation_payment_cc'] = "Kreditkarte";
$lang['donation_payment_paypal'] = "PayPal";
$lang['donation_payment_eps'] = "EPS Online&uuml;berweisung";
$lang['donation_payment_cc_choose'] = "Kreditkarte w&auml;hlen ...";
$lang['donation_payment_cc_cardnummer'] = "Kartennummer";
$lang['donation_payment_cc_cvn'] = "CVV";
$lang['donation_payment_cc_exp_month'] = "MM";
$lang['donation_payment_cc_exp_year'] = "JJ";
$lang['donation_payment_paypal_notice'] = "Hinweis zur PayPal Zahlung";
$lang['donation_payment_paypal_text'] = "Sie werden am Ende des Spendenprozesses direkt zu PayPal weitergeleitet. Bitte erlauben Sie, dass ein Pop-up Fenster ge&ouml;ffnet wird, damit der Spendenvorgang abgeschlossen werden kann.";
$lang['donation_payment_eps_choose'] = "Bank w&auml;hlen ...";
$lang['donation_payment_eps_notice'] = "Sie werden am Ende des Spendenprozesses direkt zum Online Portal ihrer Bank weitergeleitet. Bitte erlauben Sie, dass ein Pop-up Fenster ge&ouml;ffnet wird, damit der Spendenvorgang abgeschlossen werden kann.";
$lang['donation_payment_personal_header'] = "Pers&ouml;nliche Daten";
$lang['donation_payment_personal_title'] = "Titel";
$lang['donation_payment_personal_firstname'] = "Vorname";
$lang['donation_payment_personal_lastname'] = "Nachname";
$lang['donation_payment_personal_phone'] = "Telefon";
$lang['donation_payment_personal_email'] = "E-Mail";
$lang['donation_payment_personal_bday_day'] = "TT";
$lang['donation_payment_personal_bday_month'] = "MM";
$lang['donation_payment_personal_bday_year'] = "JJJJ";
$lang['donation_payment_personal_street'] = "Strasse";
$lang['donation_payment_personal_street_nr'] = "Nummer";
$lang['donation_payment_personal_zip'] = "Postleitzahl";
$lang['donation_payment_personal_city'] = "Stadt";
$lang['donation_payment_personal_country'] = "Staat ausw&auml;hlen ...";
$lang['donation_send_button'] = "Spende jetzt abschicken";

$lang['search_noresults'] = "Die Suche ergab leider keine Treffer";
