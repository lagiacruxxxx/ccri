<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{
    	
    function __construct()
    {
        parent::__construct();
        
        $this->connectDB();
        $this->load->library('session');
    }  

    public function connectDB()
    {
        //var_dump(strpos(site_url(), '192.168.1.108') !== false);
        
        if( strpos(site_url(), 'localhost') !== false || (strpos(site_url(), '192.168.1') !== false && strpos(site_url(), '192.168.1.7') === false) )
            $this->load->database('development');
        elseif(strpos(site_url(), 'fileserver') !== false || strpos(site_url(), '192.168.1.7') !== false)
            $this->load->database('testing');
        else 
            $this->load->database('productive');
    }    
}
