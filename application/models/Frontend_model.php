<?php

class Frontend_model extends CI_Model  
{
    
    function getConfig($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('config');
    }
    
	function getBackgrounds()
	{
		$this->db->where('ordering !=', -1);
		$this->db->order_by('ordering', 'asc');
		return $this->db->get('background');
	}
	
	function getMetatags()
	{
	    return $this->db->get('news_metatag');
	}
	
	function getMetatagByName($name)
	{
	    $this->db->where('name_de', $name);
	    return $this->db->get('news_metatag');
	}
	
	function getMainmenuItems()
	{
	    return $this->db->get('mainmenu_item');
	}
	
	function getSubmenuItemsByMainID($main_id)
	{
	    $this->db->where('mainmenu_id', $main_id);
	    $this->db->where('active', 1);
	    $this->db->order_by('ordering', 'asc');
	    $this->db->order_by('name_de', 'asc');
	    return $this->db->get('submenu_item');
	}
	
	function getNewsArticles()
	{
	    $this->db->where('ordering !=', -1);
	    $this->db->where('active', 1);
	    $this->db->order_by('ordering');
	    return $this->db->get('news_article');
	}
	
	function getCountries()
	{
	    $this->db->order_by('priority', 'asc');
	    $this->db->order_by('name_de', 'asc');
	    return $this->db->get('country');
	}
	
	function getNewsArticlesByMetatagID($id)
	{
	    
	    $this->db->from('news_article');
	    $this->db->join('news_article_metatag', 'news_article_metatag.news_article_id=news_article.id');
	    $this->db->where('news_article_metatag.news_metatag_id', $id);
	    $this->db->where('news_article.active', 1);
	    $this->db->order_by('news_article.created_date', 'desc');
	    return $this->db->get();
	}
	
	function getNewsArticleByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('news_article');
	}
	
	function getNewsArticlesBySubsite($id)
	{
	    
	    $this->db->from('news_article');
	    $this->db->join('news_subsite_relation', 'news_subsite_relation.news_article_id=news_article.id');
	    $this->db->where('news_subsite_relation.subsite_id', $id);
	    $this->db->where('news_article.active', 1);
	    $this->db->order_by('news_article.created_date', 'desc');
	    return $this->db->get();
	}
	
	function getNewsArticleByPrettyURL($prettyurl)
	{
	    $this->db->select('news_article.*');
	    $this->db->where('prettyurl', $prettyurl);
	    return $this->db->get('news_article');
	}
	
	function getContentByModule($parent_id, $parent_type, $table)
	{
	    $this->db->where('parent_type', $parent_type);
	    $this->db->where('parent_id', $parent_id);
	    return $this->db->get($table);
	}
	
	// BACKEND + FRONTEND
	function getColors()
	{
	    return $this->db->get('color');
	}

	function getSubsiteByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('subsite');
	}
	
	function getSubsiteByPrettyURL($pretty)
	{
	    $this->db->where('prettyurl', $pretty);
	    return $this->db->get('subsite');
	}
	
	function insertNewsletterEmail($email)
	{
	    $this->db->insert('newsletter_email', array('email' => $email));
	}
	
	
	public function getGalleryModuleImages($module_id)
	{
	
	    $this->db->where('gallery_module_id', $module_id);
	    return $this->db->get('module_gallery_item');
	}
	
	public function searchSubsiteByName($like)
	{
		$this->db->like('name', $like); 
		return $this->db->get('subsite');
	}
	public function searchArticleByName($like)
	{
		$this->db->like('headline', $like); 
		$this->db->or_like('teaser_text', $like);
		$this->db->where('id !=' . UNIQUE_TOYSTORE_NEWS_ARTICLE_ID);
		return $this->db->get('news_article');
	}
	public function searchMetatagByName($like)
	{
		$this->db->like('name_de', $like); 
		$this->db->or_like('name_en', $like);
		return $this->db->get('news_metatag');
	}
	
	public function searchTextModuleByName($like)
	{
		$this->db->like('content', $like); 
		return $this->db->get('module_text');
	}
	
	public function searchBulletpointModuleByName($like)
	{
	    $this->db->like('content', $like);
	    return $this->db->get('module_bulletpoint');
	}
}
?>