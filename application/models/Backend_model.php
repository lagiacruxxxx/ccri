<?php

class Backend_model extends CI_Model
{
    function getCountryByID($country_id)
    {
        $this->db->where('id', $country_id);
        return $this->db->get('country');
    }
    
    
    function getUsers()
    {
        return $this->db->get('user');        
    }
    
    /**************************************************************************************************************
    * BACKGROUNDS
    ***************************************************************************************************************/
    function getBackgroundsActive()
    {
        $this->db->order_by('ordering, name');
        $this->db->where('ordering !=', -1);
        return $this->db->get('background');
    }    
    
    function getBackgroundsAvailable()
    {
        $this->db->order_by('ordering, name');
        $this->db->where('ordering', -1);
        return $this->db->get('background');
    } 
    
    function resetBackgrounds()
    {
        $this->db->trans_start();
        $this->db->set('ordering', -1);
        $this->db->update('background');
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
            return false;
        else
            return true;        
    }
    
    function saveBackgroundOrdering($array, $key)
    {
        $this->db->update_batch('background', $array, $key);
        
        return true;        
    }
    
    function getNewsArticles()
    {
        return $this->db->get('news_article');
    }
    
    /**************************************************************************************************************
     * FRONTPAGE ARTICLES
     ***************************************************************************************************************/    
    function getFrontpageArticlesAvailable()
    {
        $this->db->where('ordering', -1);
        $this->db->where('active', 1);
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('news_article');
    }

    function getFrontpageArticlesActive()
    {
        $this->db->where('ordering !=', -1);
        $this->db->where('active', 1);
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('news_article');
    }
    
    function resetFrontpageArticles()
    {
        $this->db->trans_start();
        $this->db->set('ordering', -1);
        $this->db->update('news_article');
        $this->db->trans_complete();
    
        if ($this->db->trans_status() === FALSE)
            return false;
        else
            return true;
    }
    
    function saveFrontpageArticles($array, $key)
    {
        $this->db->update_batch('news_article', $array, $key);
    
        return true;
    }    

    

    /**************************************************************************************************************
     * CONTENT EDIT
     ***************************************************************************************************************/
	public function deleteContentPerModule($parent_id, $parent_type, $table)
	{
	    $this->db->trans_start();
		$this->db->where('parent_id', $parent_id);
		$this->db->where('parent_type', $parent_type);
		$this->db->delete($table);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		    return false;
	    else
	        return true;
	}
	
	public function saveContentPerModule($module, $table)
	{
	    $this->db->trans_start();
        $this->db->insert($table, $module);

        $this->db->trans_complete();
    
        if ($this->db->trans_status() === FALSE)
            return false;
        else
            return true;
	}
	
	public function saveContentPerModuleGallery($module, $table)
	{
	    $this->db->trans_start();
        $this->db->insert($table, $module);
		$last = $this->db->insert_id();
        $this->db->trans_complete();
    
        if ($this->db->trans_status() === FALSE)
            return false;
        else
            return $last;
	}	
	
	
	public function saveGalleryImage($data)
	{
	    $this->db->trans_start();
        
		$this->db->insert('module_gallery_item', $data);
	       
        $this->db->trans_complete();
    
        if ($this->db->trans_status() === FALSE)
            return false;
        else
            return true;
	}
	
	public function getContentPerModule($parent_id, $parent_type, $table)
	{
	    $this->db->where('parent_id', $parent_id);
	    $this->db->where('parent_type', $parent_type);
	    $this->db->order_by('ordering', 'asc');
	    return $this->db->get($table);
	}
	
	
	public function getImagesPerGalleryModule($module_id)
	{
	
	    $this->db->where('gallery_module_id', $module_id);
	    return $this->db->get('module_gallery_item');
	}
	
	// BACKEND + FRONTEND
	public function getColors()
	{
	    return $this->db->get('color');
	}
	
	public function getTextTemplates()
	{
	    return $this->db->get('text_templates');
	}
	
	public function getArticleByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('news_article');
	}
	
	public function getSubsiteByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('subsite');
	}	
	
	public function getNewsArticleByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('news_article');
	}
	
	
	/**************************************************************************************************************
	 * MENUS
	 ***************************************************************************************************************/
	public function getMainMenuItems()
	{
	    return $this->db->get('mainmenu_item');
	}
	
	public function getSubsites()
	{
	    return $this->db->get('subsite');
	}
	
	
	public function getNewsletterRegistrations()
	{
	    return $this->db->get('newsletter_email');
	}
}

?>