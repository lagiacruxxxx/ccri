<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class St_anna extends MY_Controller 
{
    protected $besc_config = null;
    
    function __construct()
    {
        parent::__construct();
        
        date_default_timezone_set('Europe/Vienna');
        
        $this->config->load('st_anna');
        $this->lang->load('frontend', 'german');
        $this->load->model('Frontend_model', 'fm');
        
        $this->besc_config = $this->fm->getConfig($this->config->item('st_anna_config_id'))->row();
    }  


	/**********************************************************************************************************************************************************************************
	 * GENERAL
	 *********************************************************************************************************************************************************************************/
	
    public function index()
    {
        $data['news_articles'] = $this->news_articles();
        $this->default_view('frontend/home', $data);
    }
    
    
    public function default_view($view, $viewdata, $headerData = array())
    {
        
        $data = $this->header_info();
        $data = array_merge($headerData, $data);
        $this->load->helper('cookie');
        $data['show_cookie_warning'] = get_cookie('cookie_accepted') != NULL ? get_cookie('cookie_accepted') : 0;
        
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/header', $data);
        $this->load->view($view, $viewdata);
        $this->load->view('frontend/footer', $data);
    }    
    
	protected function header_info()
	{
	    if(!$this->agent->is_mobile() || $this->agent->is_mobile('ipad'))
	        $data['is_mobile'] = false;
	    else
	        $data['is_mobile'] = true;
	     
	    
	    $data['besc_config'] = $this->besc_config;
	    $data['contactPrettyurl'] = $this->fm->getSubsiteByID($this->besc_config->contact_subsite)->row()->prettyurl;
	    $data['impressPrettyurl'] = $this->fm->getSubsiteByID($this->besc_config->impress_subsite)->row()->prettyurl;
	    $data['privacyPrettyurl'] = $this->fm->getSubsiteByID($this->besc_config->privacy_subsite)->row()->prettyurl;
	    $data['backgrounds'] = $this->backgrounds();
	    $data['submenus'] = $this->submenus();
	    
	    return $data;
	}
	
	protected function submenus()
	{
	    $submenus = array();
	    
	    $mainmenu_items = $this->fm->getMainmenuItems();
	    foreach($mainmenu_items->result() as $item)
	    {
	        switch($item->id)
	        {
	            case MAINMENU_HOME:
	            case MAINMENU_CONTACT:
	                break;
	            case MAINMENU_NEWS:
	                $metatags = array();
	                foreach($this->fm->getMetatags()->result() as $tag)
	                {
	                    $articles = $this->fm->getNewsArticlesByMetatagID($tag->id);
	                    if(count($this->filter_news_articles_by_date($articles)) > 0)
	                        $metatags[] = $tag;
	                }
	                
	                $submenus[$item->id]['items'] = $metatags;
	                $submenus[$item->id]['is_filter'] = true;
	                $submenus[$item->id]['count'] = count($metatags);
	                break;
	            case MAINMENU_ABOUT:
                case MAINMENU_RESEARCH:
	            case MAINMENU_DONATE:
	                $sub = $this->fm->getSubmenuItemsByMainID($item->id);
	                foreach($sub->result() as $s)
	                {
	                    switch($s->target)
	                    {
	                        case UNIQUE_DONATE_PAGE_ID:
	                            $prettyurl = UNIQUE_DONATE_PAGE_PRETTYURL;
	                            break;
	                        case UNIQUE_TOYSTORE_PAGE_ID:
	                            $prettyurl = UNIQUE_TOYSTORE_PAGE_PRETTYURL;
	                            break;
	                        default:
	                            $prettyurl = $this->fm->getSubsiteByID($s->target)->row()->prettyurl;
	                    }
	                    
	                    $submenus[$item->id]['items'][$s->id] = array(
                            'name' => $s->name_de,
	                        'target' => $prettyurl,    
	                    );
	                }
	                // = $this->fm->getSubmenuItemsByMainID($item->id)->result();
	                $submenus[$item->id]['is_filter'] = false;
	                $submenus[$item->id]['count'] = $sub->num_rows();
	                break;
	        }
	        
	        if($item->id != MAINMENU_HOME && $item->id != MAINMENU_CONTACT)
	        {
	            $is_unique = false;
	            foreach($this->config->item('st_anna_unique_pages') as $unique)
	            {
	                if($unique['id'] == $item->subsite_id)
	                {
	                    $is_unique = true;
	                    switch($item->subsite_id)
	                    {
	                        case UNIQUE_DONATE_PAGE_ID:
                                $submenus[$item->id]['img'] = $item->image;
                                $submenus[$item->id]['id'] = $item->id;
	                            $submenus[$item->id]['subsite_id'] = UNIQUE_DONATE_PAGE_ID;
	                            $submenus[$item->id]['prettyurl'] = UNIQUE_DONATE_PAGE_PRETTYURL;
	                            break;
	                        case UNIQUE_TOYSTORE_PAGE_ID:
                                $submenus[$item->id]['img'] = $item->image;
                                $submenus[$item->id]['id'] = $item->id;
	                            $submenus[$item->id]['subsite_id'] = UNIQUE_TOYSTORE_PAGE_ID;
	                            $submenus[$item->id]['prettyurl'] = UNIQUE_TOYSTORE_PAGE_PRETTYURL;
	                            break;
	                    }
	                }
	            }
	            
	            if($is_unique == false)
	            {
	                $submenus[$item->id]['img'] = $item->image;
                    $submenus[$item->id]['id'] = $item->id;
                    $submenus[$item->id]['subsite_id'] = $item->subsite_id;
                    if($item->subsite_id != 0)
                        $submenus[$item->id]['prettyurl'] = $this->fm->getSubsiteByID($item->subsite_id)->row()->prettyurl;
                    else
                        $submenus[$item->id]['prettyurl'] = "";
	            }
	            
	        }
	    }
	    
        return $submenus;	    
	}
	
	
	protected function backgrounds()
	{
	    $bgs_fixed = array();
        $bgs = $this->fm->getBackgrounds();
	    foreach($bgs->result() as $bg)
	    {
	        $bgs_fixed[$bg->id]['bg_img'] = $bg->bg_img;
	        $bgs_fixed[$bg->id]['bg_img_blurred'] = $bg->bg_img_blurred;
	        $bgs_fixed[$bg->id]['img'] = $bg->img;
	        $bgs_fixed[$bg->id]['video_embedd'] = $bg->video_embedd;
	        $bgs_fixed[$bg->id]['ordering'] = $bg->ordering;
	        $bgs_fixed[$bg->id]['id'] = $bg->id;
	        if($bg->news_article_id != 0)
	        {
	            $article = $this->fm->getNewsArticleByID($bg->news_article_id);
	            $bgs_fixed[$bg->id]['news_article_id'] = $article->row()->prettyurl;
	        }
	        else
	            $bgs_fixed[$bg->id]['news_article_id'] = null;
	        
	        if($bg->text_content != "")
	        {
	            $bgs_fixed[$bg->id]['bg_text']['style'] = "";
	            switch($bg->text_orientation_x)
	            {
	                default:
	                case TEXT_ORIENTATION_LEFT:
	                    $bgs_fixed[$bg->id]['bg_text']['style'] .= "text-align: left;";
	                    break;
	                case TEXT_ORIENTATION_RIGHT:
	                    $bgs_fixed[$bg->id]['bg_text']['style'] .= "text-align: right;";
	                    break;
	                case TEXT_ORIENTATION_CENTER:
	                    $bgs_fixed[$bg->id]['bg_text']['style'] .= "text-align: center;";
	                    break;
	            }
	            
	            switch($bg->text_orientation_y)
	            {
	                default:
	                case TEXT_ORIENTATION_TOP:
	                    $bgs_fixed[$bg->id]['bg_text']['style'] .= "vertical-align: top;";
	                    break;
	                case TEXT_ORIENTATION_BOTTOM:
	                    $bgs_fixed[$bg->id]['bg_text']['style'] .= "vertical-align: bottom;";
	                    break;
	                case TEXT_ORIENTATION_MIDDLE:
	                    $bgs_fixed[$bg->id]['bg_text']['style'] .= "vertical-align: middle;";
	                    break;
	            }
	            
                $bgs_fixed[$bg->id]['bg_text']['strings'] = explode("\n", $bg->text_content);
	        }
	        else
	        {
                $bgs_fixed[$bg->id]['bg_text']['style'] = "";
                $bgs_fixed[$bg->id]['bg_text']['strings'] = "";
	        }
	    }
	    
	    return $bgs_fixed;
	}
	
	/**********************************************************************************************************************************************************************************
	 * GLOBAL CONTENT
	 *********************************************************************************************************************************************************************************/
	
	public function getColors()
	{
	    $colors = array();
	    foreach($this->fm->getColors()->result() as $color)
	    {
	        $colors[$color->id] = array
	        (
	            'id' => $color->id,
	            'name' => $color->name,
	            'hexcode' => $color->hexcode,
	        );
	    }
	     
	    return $colors;
	}
	
	public function getModules($parent_id, $parent_type)
	{
	    $moduletypes = $this->config->item('st_anna_module_types');
	    $modules = array();
	    $colors = $this->getColors();
	    foreach($moduletypes as $type => $table)
	    {
	        foreach($this->fm->getContentByModule($parent_id, $parent_type, $table)->result() as $module)
	        {
	            switch($type)
	            {
	                case 'text':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/text', array('module' => $module, 'colors' => $colors), true);
	                    break;
                    case 'image':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/image', array('module' => $module), true);
	                    break;
	                case 'gallery':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/gallery', array('module' => $module, 'images' => $this->fm->getGalleryModuleImages($module->id)->result()), true);
	                    break;
	                case 'download':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/download', array('module' => $module), true);
	                    break;
	                case 'bulletpoint':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/bulletpoint', array('module' => $module, 'colors' => $colors), true);
	                    break;
	            }
	        }
	    }
	    ksort($modules);
	     
	    return $modules;
	}
	
	protected function sidebar($img, $link, $parent_type, $parent_id)
	{
	    $data['img'] = $img;
	    $data['link'] = $link;
	    $data['text_images'] = array();
	    
	    $moduletypes = $this->config->item('st_anna_module_types');
	    foreach($this->fm->getContentByModule($parent_id, $parent_type, $moduletypes['text'])->result() as $module)
	    {
	        if($module->right_side_img != '')
	        {
	            $data['text_images'][] = array(
	                'fname' => $module->right_side_img, 
	                'top' => $module->right_side_img_top,
	                'text' => $module->right_side_img_text,
	            );
	        }
	    }
	    
	    return $this->load->view('frontend/sidebar', $data, true);
	}
	
	
	/**********************************************************************************************************************************************************************************
	 * SUBSITES
	 *********************************************************************************************************************************************************************************/
	
	public function subsite($prettyurl)
	{
	    $is_unique = false;
	    foreach($this->config->item('st_anna_unique_pages') as $unique)
	    {
	        if($unique['prettyurl'] == $prettyurl)
	        {
	            $is_unique = true;
	            switch($prettyurl)
	            {
	                case UNIQUE_DONATE_PAGE_PRETTYURL:
	                    $donation_type = isset($_GET['donation_type']) ? $_GET['donation_type'] : "";
	                    $donation_amount = isset($_GET['donation_amount']) ? $_GET['donation_amount'] : "";
	                    $this->donate($donation_type, $donation_amount);
	                    break;
	                case UNIQUE_TOYSTORE_PAGE_PRETTYURL:
	                    $this->toyshop();
	                    break;
	            }
	        }
	    }
	
	    if(!$is_unique)
	    {
	        $subsite = $this->fm->getSubsiteByPrettyURL($prettyurl);
	        if($subsite->num_rows() == 1)
	        {
                $data['subsite'] = $subsite->row();
    	        $data['modules'] = $this->getModules($data['subsite']->id, MODULE_PARENT_TYPE_SUBSITE);
    	        $data['sidebar'] = $this->sidebar($data['subsite']->right_side_image, $data['subsite']->right_side_link, MODULE_PARENT_TYPE_SUBSITE, $data['subsite']->id);
    			$data['news'] = $this->fm->getNewsArticlesBySubsite($data['subsite']->id)->result();
    			
    			$headerData = array(
    			    'headerinfo' => (object)array(
    			        'pagetitle' => $data['subsite']->pagetitle,
    			        'description' => $data['subsite']->description,
    			        'keywords' => $data['subsite']->keywords,
    			        'og_image' => $data['subsite']->og_image,
    			        'url' => site_url('subsite/' . $data['subsite']->prettyurl),
    			    ),
			    );
    			
    	        $this->default_view('frontend/subsite', $data, $headerData);
	        }
	        else
	        {
	            $this->default_view('frontend/404', array());
	        }
	    }
	}	
	
	
	/**********************************************************************************************************************************************************************************
	 * NEWS ARTICLES 
	 *********************************************************************************************************************************************************************************/
	
	public function news($prettyurl)
	{
	    $prettyurl = urldecode($prettyurl);
	    $data['news'] = $this->fm->getNewsArticleByPrettyURL($prettyurl);
	    if($data['news']->num_rows() == 1)
	    {
	        $data['news'] = $data['news']->row();
	        $data['modules'] = $this->getModules($data['news']->id, MODULE_PARENT_TYPE_NEWS);
	        $data['sidebar'] = $this->sidebar($data['news']->right_side_image, $data['news']->right_side_link, MODULE_PARENT_TYPE_NEWS, $data['news']->id);

	        $headerData = array(
	            'headerinfo' => (object)array(
	                'pagetitle' => $data['news']->pagetitle,
	                'description' => $data['news']->description,
	                'keywords' => $data['news']->keywords,
	                'og_image' => $data['news']->og_image,
	                'url' => site_url('news/' . $data['news']->prettyurl),
	            ),
	        );
	        
	        $this->default_view('frontend/news', $data, $headerData);
	    }
	    else
	    {
	        show_error('article not found', 404);
	    }
	}	
	
	protected function news_articles()
	{
	    $data['news_articles'] = $this->filter_news_articles_by_date($this->fm->getNewsArticles());
	    return $data['news_articles'];
	}
	
	public function news_filtered($tag)
	{
	    $tag = urldecode($tag);
	    $metatag = $this->fm->getMetatagByName($tag);
	    if($metatag->num_rows() == 1)
	    {
	        $data['articles'] = $this->fm->getNewsArticlesByMetatagID($metatag->row()->id);
	        $data['articles'] = $this->filter_news_articles_by_date($data['articles']);
		
	        $this->default_view('frontend/news_filtered', $data);
	    }
	    else 
	    {
	        $this->load->view('errors/html/error_general', array('heading' => 'error', 'message' => 'metatag not found'));
	    }
	}
	
	protected function filter_news_articles_by_date($articles)
	{
	    $filtered_news = array();
	    $today = strtotime(date('Y-m-d'));
	    foreach($articles->result_array() as $article)
	    {
	        $start_pass = true;
	        if($article['timed_start'] != "" && $article['timed_start'] != 1 && $article['timed_start'] != "0000-00-00 00:00:00" && $article['timed_start'] != null && $article['timed_start'] != '1970-01-01 01:00:00')
	        {
	            $startdate = strtotime($article['timed_start']);
	            if($today < $startdate)
	                $start_pass = false;
	        }
	        
	        $end_pass = true;
	        if($article['timed_end'] != "" && $article['timed_end'] != 1 && $article['timed_end'] != "0000-00-00 00:00:00" && $article['timed_end'] != null && $article['timed_start'] != '1970-01-01 01:00:00')
	        {
	            $enddate = strtotime($article['timed_end']);
	            if($today < $enddate)
	                $end_pass = false;
	        }
	        	        
	        if($start_pass && $end_pass)
	            $filtered_news[] = $article;
	    }
	    
	    return $filtered_news;
	}
	
	/**********************************************************************************************************************************************************************************
	 * DONATIONS
	 *********************************************************************************************************************************************************************************/
	
	public function donate($donation_type, $donation_amount)
	{
	    $data['donation_type'] = $donation_type;
	    $data['donation_amount'] = $donation_amount;
	    $data['countries'] = $this->fm->getCountries();
	    $this->default_view('frontend/donate', $data);
	}	
	
	public function toyshop()
	{
	    $data['countries'] = $this->fm->getCountries();
	    $this->default_view('frontend/toyshop', $data);
	}	
	
	/**********************************************************************************************************************************************************************************
	 * NEWSLETTER
	 *********************************************************************************************************************************************************************************/
	public function save_newsletter_email()
	{
	    $email = $_POST['email'];
	    $this->fm->insertNewsletterEmail($email);
	}
	
	/**********************************************************************************************************************************************************************************
	 * SEARCH
	 *********************************************************************************************************************************************************************************/
	public function search()
	{
		$result_container['subsite'] = array();
		$result_container['article'] = array();
		
	    $text = $_GET['param'];

	    $subsite_results = $this->fm->searchSubsiteByName($text)->result();
	    $article_results = $this->fm->searchArticleByName($text)->result();
	    $metatag_results = $this->fm->searchMetatagByName($text)->result();
	    $text_module_results = $this->fm->searchTextModuleByName($text)->result();
	    $bulletpoint_module_results = $this->fm->searchBulletpointModuleByName($text)->result();
	   
	    foreach($text_module_results as $result)
	    {
	        $result_item = null;
	   		if($result->parent_type == 0)
	   		{
	   			$article = $this->fm->getNewsArticleByID($result->parent_id)->row();
	   			
	   			if(isset($article))
	   			{
	   			    
	   			    $result_item = array(
	   			        'id' => $article->id,
   						'headline' => $article->headline,
   						'teaser_text' => $article->teaser_text,
   						'created_date' => $article->created_date,
   						'prettyurl' => $article->prettyurl,
   						'teaser_img' => $article->teaser_img,
   						'type' => 'article');
		   		}
	   		}
            else    
	   		{
	   			$subsite = $this->fm->getSubsiteByID($result->parent_id)->row();
	   			
	   			if(isset($subsite))
	   			{
    	   			$teaser_text = $result->content;
    		   		$result_item = array(
    		   		    'id' => $subsite->id,
    					'name' => $subsite->name,
    					'teaser_text' => nl2br($result->content),
    					'prettyurl' => $subsite->prettyurl,
    					'type' => 'subsite');
	   			}
	   		}
	   		
	   		if($result_item != null)
	   		{
	   		    if(!isset($result_container[$result_item['type']][$result_item['id']]))
            	   	$result_container[$result_item['type']][$result_item['id']] = $result_item;
	   		}				
	   }
	   
	   foreach($subsite_results as $result)
	   {
	   		$content = $this->fm->getContentByModule($result->id, 1, 'module_text')->row()->content;
	   		
            $result_item = array(
                'id' => $result->id,
				'name' => $result->name,
				'teaser_text' => $content,
				'prettyurl' => $result->prettyurl,
				'type' => 'subsite');
            
            if(!isset($result_container[$result_item['type']][$result_item['id']]))
                $result_container[$result_item['type']][$result_item['id']] = $result_item;				
	   }
	   
	   foreach($article_results as $result)
	   {
            $result_item = array(
                'id' => $result->id,
				'headline' => $result->headline,
				'teaser_text' => $result->teaser_text,
				'created_date' => $result->created_date,
				'prettyurl' => $result->prettyurl,
				'teaser_img' => $result->teaser_img,
				'type' => 'article');
            
            
            if(!isset($result_container[$result_item['type']][$result_item['id']]))
                $result_container[$result_item['type']][$result_item['id']] = $result_item;

	   }
	   
	   foreach($metatag_results as $result)
	   {
	       $connected_articles = $this->fm->getNewsArticlesByMetatagID($result->id)->result();
	       
		   foreach($connected_articles as $article)
		   {
                $result_item = array(
                    'id' => $article->news_article_id,
                    'headline' => $article->headline,
					'prettyurl' => $article->prettyurl,
					'created_date' => $article->created_date,
					'teaser_text' => $article->teaser_text,
					'teaser_img' => $article->teaser_img,
					'type' => 'article');
                
                if(!isset($result_container[$result_item['type']][$result_item['id']]))
                    $result_container[$result_item['type']][$result_item['id']] = $result_item;				
		   }
		   		
        }

        $data['results'] = $result_container;
	    
        $this->default_view('frontend/search_result', $data);
	}
	
}