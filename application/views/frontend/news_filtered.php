
    
        <script>
            var ignite_backgrounds = 0;
        </script>


        <div id="container" class="containerborder containerwidth filtered_articles">
                <?php foreach($articles as $article):?>
                <div class="article_preview">
                    <div class="article_preview_headline"><a href="<?= site_url('news/' . $article['prettyurl'])?>"><?= $article['headline']?></a></div>
                    <div class="article_preview_dataline">
                        <div class="article_preview_date"><?= date('d.m.Y', strtotime($article['created_date']))?></div>
                    </div>
                    <div class="article_preview_contentline">
                        <a href="<?= site_url('news/' . $article['prettyurl'])?>">
                            <div class="article_preview_image">
                                <img src="<?= site_url('items/general/uploads/article_teaser/' . $article['teaser_img'])?>" title="<?= $article['alt_text']?>" />
                            </div>
                        </a>
                        <div class="article_preview_text">
                            <?= $article['teaser_text'] ?>
                            <div class="article_preview_readmore">
                                <a href="<?= site_url('news/' . $article['prettyurl'])?>"><?= $this->lang->line('newsarticle_readmore')?></a>
                            </div>
                    </div> 
                    </div>                        
                    
                </div>
            <?php endforeach;?>
        </div>