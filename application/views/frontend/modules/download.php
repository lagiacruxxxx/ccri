

        <div class="module module_download" style="margin-top: <?= $module->margin_top?>px; margin-bottom: <?= $module->margin_bottom?>px;">
            <div class="module_download_text">
                <?= $module->text?>
            </div>
            <a href="<?= site_url('items/general/uploads/content_downloads/' . $module->fname)?>" target="_blank">
                <div class="module_download_button"></div>
            </a>    
        </div>