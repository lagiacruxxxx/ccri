<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<? if($is_mobile):?>
		<meta name="viewport" content="width=320px, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no">
	<? endif;?>	
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
    <meta charset="UTF-8">
	
	<meta property="og:title" content="<?php if(isset($headerinfo) && $headerinfo->pagetitle != '') echo $headerinfo->pagetitle; else echo "St. Anna Kinderkrebsforschung";?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="<?php if(isset($headerinfo) && $headerinfo->url != '') echo $headerinfo->url; else echo "http://www.kinderkrebsforschung.at/";?>"/>
	<meta property="og:image" content="<?php if(isset($headerinfo) && $headerinfo->og_image != '') echo site_url('items/general/uploads/ogimage/' . $headerinfo->og_image); else echo site_url('items/frontend/img/fb_share.png');?>"/>
	<meta property="og:description" content="<?php if(isset($headerinfo) && $headerinfo->description != '') echo $headerinfo->description; else echo "St. Anna Kinderkrebsforschung";?>"/>
	<meta name="description" content="<?php if(isset($headerinfo) && $headerinfo->description != '') echo $headerinfo->description; else echo "St. Anna Kinderkrebsforschung";?>"/>
	<meta name="keywords" content="<?php if(isset($headerinfo) && $headerinfo->keywords != '') echo $headerinfo->keywords; else echo "St. Anna Kinderkrebsforschung";?>">
	<meta name="google" content="notranslate" />

	<title><?php if(isset($headerinfo) && $headerinfo->pagetitle != '') echo $headerinfo->pagetitle; else echo "St. Anna Kinderkrebsforschung";?></title>
	
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/lightbox.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/desktop.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/jquery.qtip.css"); ?>">
	<? if($is_mobile):?>
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/mobile.css"); ?>">
	<? endif;?>		

	<script type="text/javascript" src="<?=site_url("items/general/js/jquery-1.11.2.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/packery.pkgd.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/lightbox-2.6.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.qtip.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/placeholders.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/cookie.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/responsive.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/desktop.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/background.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/layouts/widescreen.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/layouts/smallscreen.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/layouts/mobiledevice.js"); ?>"></script>
		
	<!-- VARIABLES -->
	<script type="text/javascript">
            var rootUrl = "<?= site_url(); ?>";
            var bg_interval = <?= $besc_config->background_change_interval?>;
            var prettyurl_donate = "<?= UNIQUE_DONATE_PAGE_PRETTYURL?>";
            var prettyurl_toystore = "<?= UNIQUE_TOYSTORE_PAGE_PRETTYURL?>";
            var video_embedds = []; 
            <?php foreach($backgrounds as $bg):?>
                <?php if($bg['video_embedd'] != ""):?>
                    video_embedds[<?=$bg['id']?>] = "<?=$bg['video_embedd']?>";  
                <?php endif;?>
            <?php endforeach;?>
            
	</script>	
	
	<!-- YT -->
	<script>
        var tag = document.createElement('script');
    
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	</script>
	<!-- GA -->
     <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-5934189-61', 'auto');
  ga('send', 'pageview');

</script>
     
     
     
    <!-- FB -->
    
    <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '478330542363512',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

</head>
<body>
	
