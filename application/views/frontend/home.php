
        <script>
            var ignite_backgrounds = 1;
        </script>

		<div id="container" class="containerborder containerwidth">
            
            <div id="headline_container">
                <?php foreach($backgrounds as $bg):?>
                    
                    <?php if($bg['bg_text']['strings'] != ""):?>
                        <div class="headline" style="<?= $bg['bg_text']['style']?> display: none;" ordering="<?= $bg['ordering']?>">
                            <?php foreach($bg['bg_text']['strings'] as $string):?>
                                <?php if($bg['news_article_id'] != null):?>
                                    <a href="<?= site_url('news/' . $bg['news_article_id'])?>">
                                <?php endif;?>
                                <span class="headline_text bg_stanna_yellow"><?= $string?></span><br />
                                <?php if($bg['news_article_id'] != null):?>
                                    </a>
                                <?php endif;?>
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>
                    
                
                    <?php if($bg['img'] != ""):?>
                        <div class="headline_img" ordering="<?= $bg['ordering']?>">
                            <img src="<?= site_url('items/general/uploads/backgrounds_small/' . $bg['img'])?>" />
                        </div>
                    <?php endif;?>
                    
                    <?php if($bg['video_embedd'] != ""):?>
                        <div class="headline_video" ordering="<?= $bg['ordering']?>">
                            <div id="yt_iframe_<?= $bg['id']?>"></div>
                        </div>
                    <?php endif;?>
                
                <?php endforeach;?>
                
            </div>
            <div id="articles">
                <?php foreach($news_articles as $article):?>
                    
                        <?php if($article['id'] == UNIQUE_TOYSTORE_NEWS_ARTICLE_ID):?>
                        <div class="article">
                            <a href="<?= site_url('subsite/' . UNIQUE_TOYSTORE_PAGE_PRETTYURL)?>"><img class="" src="<?= site_url('items/frontend/img/zoo_kathi.png/')?>"/></a>
                        </div>
                        <?php elseif($article['id'] == UNIQUE_DONATE_NEWS_ARTICLE_ID):?>
                        <div class="article bg_stanna_yellow">
                            <!-- <div class="donate_overlay_donation_box2">
                                <div class="donate_donation_box_label2">
                                    Ich helfe
                                </div>
                                <input type="radio" class="donation_box_radio2" name="donation_type2" id="donation_type_onetime2" value="onetime" CHECKED/>
                                <label class="donation_box_radio_label2" for="donation_type_onetime2">einmalig</label><br />
                                <input type="radio" class="donation_box_radio2" name="donation_type2" id="donation_type_recurring2" value="recurring" />
                                <label class="donation_box_radio_label2" for="donation_type_recurring2">regelm&auml;ssig</label>
                                
                            </div> -->
                            <img id="donate_overlay_donate_logo" src="<?= site_url('items/frontend/img/oesgs_logo.png') ?>" style="position: relative; width: 82px; margin-bottom: 15px; margin-top: 31px; margin-right: 84px; margin-left: 84px; right: auto; bottom: auto;" />
                            <div class="donate_overlay_donation_box2">
                                <div class="donate_donation_box_label2">
                                    Meine Spende
                                </div>
                                <input type="text" class="donation_box_text2" value="0,00"/>
                                <span>EUR</span>
                            </div>
                            <div id="donate_overlay_donation_button2">
                                <?= $this->lang->line('donate_overlay_button')?>
                            </div>
                        </div>
                        <?php else:?>
                        <div class="article">
                            <div class="article_headline"><a href="<?= site_url('news/' . urlencode($article['prettyurl']))?>"><?= nl2br($article['headline']) ?></a></div>
                            <a href="<?= site_url('news/' . urlencode($article['prettyurl']))?>"><img class="article_teaserimg" src="<?= site_url('items/general/uploads/article_teaser/' . $article['teaser_img'])?>" title="<?= $article['alt_text']?>"/></a>
                            <div class="article_teasertext"><?= $article['teaser_text']?></div>
                            <div class="article_readmore"><a href="<?= site_url('news/' . urlencode($article['prettyurl']))?>"><?= $this->lang->line('newsarticle_readmore')?></a></div>
                        </div>
                        <?php endif;?>
                    </a>    
                <?php endforeach;?>
			</div>
		</div>

		