        
        
        <div id="article_sidebar">
            <?php if($img != ''):?>
                <a href="<?= site_url('subsite/' . $link)?>" >
                    <img class="sidebar_link_img" src="<?= site_url('items/general/uploads/sidebar_image/' . $img)?>" />
                </a>
            <?php endif;?>        
            
            <?php foreach($text_images as $right_img):?>
                <div class="right_side_img" style="top: <?= $right_img['top']?>px;">
                    <img src="<?= site_url('items/general/uploads/sidebar_image/' . $right_img['fname'])?>"  />
                    <div class="right_side_img_text"><?= $right_img['text']?></div>
                </div>
            <?php endforeach;?>
        </div>