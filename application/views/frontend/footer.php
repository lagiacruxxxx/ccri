

		<div id="footer">
            <div id="footer_container">
    		    <div id="footer_social">
                    <div class="footer_column">
                        <div class="footer_column_header"><?= $this->lang->line('footer_header_social')?></div>
                        <a target="_blank" style="border:0;text-decoration:none;" href="https://twitter.com/CCRIWien"><img src="<?= site_url('items/frontend/img/menu_twitter.png')?>" class="footer_social_item"></a>
                        <a target="_blank" style="border:0;text-decoration:none;" href="https://www.facebook.com/st.anna.kinderkrebsforschung"><img src="<?= site_url('items/frontend/img/menu_facebook.png')?>" class="footer_social_item"></a>
                        <a style="border:0:text-decoration:none;" href="mailto:spende@kinderkrebsforschung.at"><img src="<?= site_url('items/frontend/img/menu_mail.png')?>" class="footer_social_item"></a>
                    </div>
                    <div class="footer_column">
                        <div class="footer_column_header"><?= $this->lang->line('footer_header_newsletter')?></div>
                        <div class="footer_newsletter">
                            <input type="text" id="footer_newsletter" placeholder="Newsletter"/>
                            <div id="footer_newsletter_success">Danke!</div>
                            <div id="footer_newsletter_register">Abonnieren</div>                    
                        </div>
                    </div>
                </div>
                <div id="footer_donations">
                    <div class="footer_column">
                        <div class="footer_column_header"><?= $this->lang->line('footer_header_donateaccount')?></div>
                        <div class="footer_donations_header">Bank Austria</div>
                        <div class="footer_donations_text">IBAN: AT79 1200 0006 5616 6600<br />BIC: BKAUATWW</div>
                        <div class="footer_donations_header">Erste Bank AG</div>
                        <div class="footer_donations_text">IBAN: AT66 2011 1000 0318 3777<br />BIC: GIBAATWW</div>
                        <div class="footer_donations_header">RAIKA Wien</div>
                        <div class="footer_donations_text">IBAN: AT29 3200 0000 0620 2808<br />BIC: RLNWATWW</div>
                        <div id="footer_donations_donatenow"><a href="<?= site_url('subsite/' . UNIQUE_DONATE_PAGE_PRETTYURL)?>"><?= $this->lang->line('footer_donatenow')?></a></div>
                    </div>
                    <div class="footer_column">
                        <div class="footer_column_header"><?= $this->lang->line('footer_header_donateservice')?></div>
                        <div class="footer_donations_header">Telefon</div>
                        <div class="footer_donations_text">Tel: +43 1 404 70/4000</div>
                        <div class="footer_donations_header">Ansprechpartnerin</div>
                        <div class="footer_donations_text">
                            Mag. Andrea Prantl<br />
                            <?= safe_mailto('spende@kinderkrebsforschung.at', 'spende@kinderkrebsforschung.at')?></div>
                        <div class="footer_seals">
                            <a href="<?= site_url('news/spendengütesiegel')?>">
                                <img src="<?= site_url('items/frontend/img/oesgs_logo.png')?>" />
                                <img src="<?= site_url('items/frontend/img/absetzbar.png')?>" />
                            </a>    
                        </div>
                    </div>
                </div>
                <div id="footer_menu">
                    <ul>
                        <li class="footer_menu_item"><a href="<?= site_url('subsite/'. $impressPrettyurl )?>"><?= $this->lang->line('footer_menu_impress')?></a></li>
                        <li class="footer_menu_item"><a href="<?= site_url('subsite/'. $contactPrettyurl )?>"><?= $this->lang->line('footer_menu_contact')?></a></li>
                        <li class="footer_menu_item"><a href="<?= site_url('subsite/'. $privacyPrettyurl )?>"><?= $this->lang->line('footer_menu_privacy')?></a></li>
                     <!--   <li class="footer_menu_item footer_menu_align_right"><a href="http://science.ccri.at"><img src="<?= site_url('items/frontend/img/footer_research.png')?>" /></a></li>
                        <li class="footer_menu_item footer_menu_align_right"><a href="http://www.stanna.at/content.php?p=1&lang=de"><img src="<?= site_url('items/frontend/img/footer_hospital.png')?>" /></a></li> -->
                    </ul>
                </div>
                <div id="footer_copyright">
                    <div class="footer_copyright_header"><?= $this->lang->line('footer_copyright_header')?></div>
                    <div class="footer_copyright_text"><?= $this->lang->line('footer_copyright_text')?></div>
                </div>
		    </div>
		</div>
	</body>
</html>