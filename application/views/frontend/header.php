        
        
        <?php if($show_cookie_warning != 1):?>
            <div id="cookie_warning">
                <div id="cookie_warning_text">Wir speichern Informationen über Ihren Besuch in sogenannten Cookies. Durch die Nutzung dieser Webseite erklären Sie sich mit der Verwendung von Cookies einverstanden.</div>
                <div id="cookie_warning_button">Akzeptieren</div>
            </div>
        <?php endif;?>

		

        <!-- TOP HEADER -->
        <div id="menu_upper">
            <div id="menu_upper_container">
                <img src="<?= site_url('items/frontend/img/menu_header.png')?>" class="upper_menu_left" />   
                
                <div id="menu_upper_container_social">
                    <a target="_blank" style="border:0;text-decoration:none;" href="https://twitter.com/CCRIWien"><img src="<?= site_url('items/frontend/img/menu_twitter.png')?>" class="upper_menu_right upper_menu_social share_twitter"></a>
                    <a target="_blank" style="border:0;text-decoration:none;" href="https://www.facebook.com/st.anna.kinderkrebsforschung"><img src="<?= site_url('items/frontend/img/menu_facebook.png')?>" class="upper_menu_right upper_menu_social share_facebook"></a>
                </div>
            </div>

            <div class="logo_small">
                <a href="<?= site_url()?>"><img src="<?= site_url('items/frontend/img/logo_small.png')?>" /></a>
            </div>
        </div>
        
        <div id="menu_shadow_bottom"></div>
        
        <!-- LOGO -->
        <div id="menu_logo">
            <a href="<?= site_url()?>" >
                <img src="<?= site_url('items/frontend/img/logo_new.png') ?>" title="St. Anna Kinderkrebs Forschung"/>
            </a>
            <div id="menu_logo_shadow_bottom"></div>
            <div id="menu_logo_shadow_left"></div>
            <div id="menu_logo_shadow_right"></div>
        </div>
        
         <? if($is_mobile){?>
         <!--MOBILE  MENU -->
		<div id="mobileMenu" class="bg_stanna_yellow">
			<a href="<?= site_url()?>"><div class="mobile_menu_item"><?= $this->lang->line('menu_home')?></div></a>
			<div class="mobile_menu_item" target="<?= MAINMENU_NEWS ?>"><?= $this->lang->line('menu_news')?></div>
			<div class="mobile_menu_item" target="<?= MAINMENU_RESEARCH ?>"><?= $this->lang->line('menu_research')?></div>
			<div class="mobile_menu_item" target="<?= MAINMENU_DONATE ?>"><?= $this->lang->line('menu_donate')?></div>
			<div class="mobile_menu_item" target="<?= MAINMENU_ABOUT ?>"><?= $this->lang->line('menu_about')?></div>
			<a href="<?= site_url('subsite/'. $contactPrettyurl )?>"><div class="mobile_menu_item" target="<?= MAINMENU_CONTACT ?>"><?= $this->lang->line('menu_contact')?></div></a>
			<a href="http://science.ccri.at"><div class="mobile_menu_item" ><?= $this->lang->line('menu_science')?></div></a>
		</div>

		<!-- MOBILE SUBMENUS -->
        <? }?>
        
        
        <!-- MENU -->
		<div id="menu" class="bg_stanna_yellow">
            <?php if($is_mobile):?>
            	<div id="mobile_menu_button">MENU</div>
            <?php else:?>
                <ul id="menu_container" class="containerwidth">
    				<li><a href="<?= site_url()?>"><?= $this->lang->line('menu_home')?></a></li>
    				<li class="menu_item" target="<?= MAINMENU_NEWS ?>"><?= $this->lang->line('menu_news')?></li>
    				<li class="menu_item" target="<?= MAINMENU_RESEARCH ?>"><?= $this->lang->line('menu_research')?></li>
    				<li class="menu_item" target="<?= MAINMENU_DONATE ?>"><?= $this->lang->line('menu_donate')?></li>
    				<li class="menu_item" target="<?= MAINMENU_ABOUT ?>"><?= $this->lang->line('menu_about')?></li>
    				<li><a href="<?= site_url('subsite/'. $contactPrettyurl )?>"><?= $this->lang->line('menu_contact')?></a></li>
    				<li><a href="http://science.ccri.at"><?= $this->lang->line('menu_science')?></a></li>
    				<li class="menu_item_search"><img src="<?= site_url('items/frontend/img/icon_search.png')?>" /></li>
    			</ul>
            <?php endif;?>
		</div>

		<!-- SUBMENUS -->
		<?php if($is_mobile):?>
			<?php foreach($submenus as $submenu):?>
                <div class="mobile_submenu bg_stanna_yellow" target="<?=$submenu['id']?>">
                        <?php $i = 0; foreach($submenu['items'] as $item):?>
                            <div class="mobile_submenu_item">
                                <?php if(!$submenu['is_filter']):?>
                                <a href="<?= site_url('subsite/' . $item['target'])?>">
                                    <?php echo $item['name'] ?>
                                </a>
                                <?php else:?>
                                <a href="<?= site_url('tag/' . urlencode($item->name_de))?>">
                                    <?php echo $item->name_de ?>
                                </a>
                                <?php endif;?>
                            </div>
                        <?php $i++; endforeach;?>
                </div>
    		<?php endforeach;?>
    		
		<?php else:?>
		
		
    		<?php foreach($submenus as $submenu):?>
                <div class="submenu containerwidth" target="<?=$submenu['id']?>">
                    <?php if($submenu['img'] != ''):?>
                    <div class="submenu_img">
                        <?php if($submenu['subsite_id'] != 0):?>
                            <a href="<?= site_url('subsite/' . $submenu['prettyurl'])?>">
                        <?php endif;?>
                        <img src="<?= site_url('items/general/uploads/menu_images/' . $submenu['img']) ?>" />
                        <?php if($submenu['subsite_id'] != 0):?>
                            </a>
                        <?php endif;?>
                    </div>
                    <?php endif;?>
                    <ul>
                        <?php $i = 0; foreach($submenu['items'] as $item):?>
                            
                            <?php if($submenu['count'] > 10 && $i == ceil($submenu['count']/2)):?>
                            </ul><ul>
                            <?php endif;?>
                        
                            <li>
                                <?php if(!$submenu['is_filter']):?>
                                <a href="<?= site_url('subsite/' . $item['target'])?>">
                                    <?php echo $item['name'] ?>
                                </a>
                                <?php else:?>
                                <a href="<?= site_url('tag/' . $item->name_de)?>">
                                    <?php echo $item->name_de ?>
                                </a>
                                <?php endif;?>
                            </li>
                        <?php $i++; endforeach;?>
                    </ul>
                </div>
    		<?php endforeach;?>
    	<?php endif;?>
		
		
		
		
        <!-- BACKGROUNDS -->
		<?php if(!$is_mobile):?>
    		<div id="bg_image">
    			<?php foreach($backgrounds as $bg):?>
    			    <?php if($bg['bg_img_blurred'] == ''):?>
                        <img src="<?= site_url('items/general/uploads/backgrounds/' . $bg['bg_img'])?>" ordering="<?= $bg['ordering']?>" />
                    <?php else:?>
                        <img src="<?= site_url('items/general/uploads/backgrounds_blurred/' . $bg['bg_img_blurred'])?>" ordering="<?= $bg['ordering']?>" />
                    <?php endif;?>
    			<?php endforeach;?>
    		</div>  
		<?php endif;?>
		
		
		<div id="donate_overlay">
            <div id="donate_overlay_preview">
                <?= $this->lang->line('donate_overlay_preview')?>              
            </div>
             <div id="donate_overlay_donation" style="width: 225px;">
                <div id="donate_overlay_donation_headline">
                    <?= $this->lang->line('donate_overlay_header')?>
                </div>
                <div id="donate_overlay_donation_box_container">
                    <!-- <div class="donate_overlay_donation_box">
                        <div class="donate_donation_box_label">
                            Ich helfe
                        </div>
                        <input type="radio" class="donation_box_radio" name="donation_type" id="donation_type_onetime" value="onetime" CHECKED/>
                        <label class="donation_box_radio_label" for="donation_type_onetime">einmalig</label><br />
                        <input type="radio" class="donation_box_radio" name="donation_type" id="donation_type_recurring" value="recurring" />
                        <label class="donation_box_radio_label" for="donation_type_recurring">regelm&auml;ssig</label>
                        
                    </div>  -->
                    <div class="donate_overlay_donation_box" style="float: none; margin-left: 10px;">
                        <div class="donate_donation_box_label">
                            Meine Spende
                        </div>
                        <input type="text" class="donation_box_text" value="0,00"/>
                        <span>EUR</span>
                    </div>
                </div>
                <div id="donate_overlay_donation_button" style="margin-left: 10px; width: 145px;">
                    <?= $this->lang->line('donate_overlay_button')?>
                </div>
                <img id="donate_overlay_donate_logo" src="<?= site_url('items/frontend/img/oesgs_logo.png') ?>" style="bottom: 100px; right: 15px; width: 60px;" />
            </div>
		</div>
		<div class="donate_overlay_button" style="top: 460px;">
            <a href="<?= site_url('subsite/' . UNIQUE_TOYSTORE_PAGE_PRETTYURL)?>">
                <img style="width:100%;" src="<?= site_url('items/frontend/img/icon_kathi.png')?>" / title="Kuschelzoo">
            </a>
        </div>
		<div class="donate_overlay_button" style="top:530px;">
            <a href="http://www.labdia.at/" target="_blank">
                <img style="width:100%;" src="<?= site_url('items/frontend/img/footer_research.png')?>" / title="labdia Labordiagnostik">
            </a>
        </div>
        <div class="donate_overlay_button" style="top:600px;">
            <a href="http://www.stanna.at/content.php?p=1&lang=de" target="_blank">
                <img style="width:100%;" src="<?= site_url('items/frontend/img/hospital.png')?>" title="St. Anna Kinderspital"/>
            </a>
        </div>
        
        <div class="donate_overlay_button" style="top:670px;">
            <a href="http://science.ccri.at" target="_blank">
                <img style="width:100%;" src="<?= site_url('items/frontend/img/dna_icon.png')?>" title="Science"/>
            </a>
        </div>
        
        
        <div id="search_overlay">
            <input type="text" value="" id="search_input">
        </div>  
		