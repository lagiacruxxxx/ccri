
        <script>
            var ignite_backgrounds = 0;
            var is_toyshop = 1;
        </script>


        <div id="container" class="containerborder containerwidth container_donation">
        
            <div id="toyshop_headline">Kuscheltiere kaufen und helfen!</div>
            <div id="toyshop_subheadline">Wir sind ca. 20 cm groß und freuen uns schon bald bei Euch zu sein!</div>
            <br />
            <div id="toyshop_subheadline">Wählen Sie ein oder mehrere Kuscheltiere aus und geben Sie anschließend bitte Ihre Daten sowie Bezahlart ein um die Bestellung abzuschließen.</div>
        
            <div id="pet_container">
                <div class="toy">
                    <div class="toy_image">
                        <img src="<?= site_url('items/general/uploads/toys/kathi.png')?>" />
                    </div>
                    <div class="toy_desc">
                        <div class="toy_text">
                            <b>Kathi</b>, das kecke Kälbchen, ist eine kleine „Lebensretterin“!
                        </div>
                        <div class="toy_orderinfo">
                            <div class="toy_price">
                                EUR 12,- / Stk.    
                            </div>
                            <div class="toy_orderamount">
                                <input type="text" class="toy_input" toy_name="Kathi" name="toy_orderamount_3" id="toy_orderamount_3" value="0"/>
                                <label for="toy_orderamount_3">Anzahl</label>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="toy">
                    <div class="toy_image">
                        <img src="<?= site_url('items/general/uploads/toys/fred.jpg')?>" />
                    </div>
                    <div class="toy_desc">
                        <div class="toy_text">
                            <b>Fred</b>, die freche Feldmaus, kommt gerne in dein Haus!
                        </div>
                        <div class="toy_orderinfo">
                            <div class="toy_price">
                                EUR 12,- / Stk.    
                            </div>
                            <div class="toy_orderamount">
                                <input type="text" class="toy_input" toy_name="Fred" name="toy_orderamount_1" id="toy_orderamount_1" value="0"/>
                                <label for="toy_orderamount_1">Anzahl</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="toy">
                    <div class="toy_image">
                        <img src="<?= site_url('items/general/uploads/toys/kiki.jpg')?>" />
                    </div>
                    <div class="toy_desc">
                        <div class="toy_text">
                            <b>Kiki</b>, das kuschelige Küken, wäre gern bei dir!
                        </div>
                        <div class="toy_orderinfo">
                            <div class="toy_price">
                                EUR 12,- / Stk.    
                            </div>
                            <div class="toy_orderamount">
                                <input type="text" class="toy_input" toy_name="Kiki" name="toy_orderamount_2" id="toy_orderamount_2" value="0"/>
                                <label for="toy_orderamount_2">Anzahl</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div id="donation_extra">
                <span>Hier können sie noch eine kleine Extra-Spende vermerken:</span>
                <input type="text" class="toy_extra_amount" value="0"/>
                <span style="float: right;">EUR</span>
            </div>
            
            <div id="donation_summary">
                <span>Der Gesamtbetrag beläuft sich auf: </span>
                <div id="donation_total">0</div>
                <span style="float: right"> EUR</span>
            </div>    
            
            <div id="donation_infotext">
                * Wir bitten um Verständnis, dass der Versand ins Ausland erst nach eingegangener Bezahlung erfolgen kann. Ebenso werden die Portokosten in Rechnung gestellt.
            </div>    
        
            <div id="donation_widget" class="dds-widget-container">
            </div>
        </div>
        
        
        <script type="text/javascript" src="<?=site_url("items/frontend/js/widget.js"); ?>"></script>
        
        <script src="https://widget.raisenow.com/widgets/lema/kinderkrebsat-maskottchen/js/dds-init-widget-de.js" type="text/javascript"></script>
        
    	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/widget.css"); ?>">
    