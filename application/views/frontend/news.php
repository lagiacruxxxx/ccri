

        <script>
            var ignite_backgrounds = 0;
        </script>


		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/modules.css"); ?>">
		
		<div id="article">
        
            <div id="article_content">
                <?php foreach($modules as $module):?>
                    <?= $module?>
                <?php endforeach;?>
                
                <div id="article_sharing">
                    <ul>
                        <li id="article_sharing_text">Teilen</li>
                        <li><img src="<?= site_url('items/frontend/img/menu_twitter.png')?>" class="footer_social_item" share="twitter"></li>
                        <li><img src="<?= site_url('items/frontend/img/menu_facebook.png')?>" class="footer_social_item" share="facebook"></li>
                        <li><img src="<?= site_url('items/frontend/img/menu_mail.png')?>" class="footer_social_item" share="mail"></li>
                    </ul>
                    <span id="article_createddate"><?= date('d.m.Y', strtotime($news->created_date))?></span>
                </div>
            </div>
            
            <?= $sidebar?>
            
		</div>

		