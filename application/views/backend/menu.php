<div id="messagecontainer"></div>

<div id="menu">
	<ul>
		<li><a href="<?= site_url('authentication/logout')?>">Logout</a></li>
		<li>Logged in as <b><?= $username?></b></li>
	</ul>
</div>

<div id="sidebar">
	<ul>
		<li class="sidebar_headline">NEWS</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('backend/news_article')?>">Articles</a>
        </li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('backend/news_metatag')?>">Metatags</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('backend/frontpage_articles')?>">Frontpage newsarticles</a>
        </li>

		<li><div class="separator"></div></li>
		<li class="sidebar_headline">Menus</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('backend/mainmenu')?>">Main menu</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('backend/submenu')?>">Sub menu</a>
        </li>
        
        <li><div class="separator"></div></li>
		<li class="sidebar_headline">Subsites</li>
		<li class="sidebar_menuitem">
            <a href="<?= site_url('backend/subsites')?>">Subsites</a>
        </li>
        
        <li><div class="separator"></div></li>
        <li class="sidebar_headline">Settings</li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('backend/backgrounds')?>">Backgrounds</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('backend/colors')?>">Colors</a>
        </li>
         <li class="sidebar_menuitem">
            <a href="<?= site_url('backend/text_templates')?>">Text templates</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('backend/settings/edit/1')?>">Settings</a>
        </li>
        <li class="sidebar_menuitem">
            <a href="<?= site_url('backend/newsletter_emails')?>">Newsletter E-Mails</a>
        </li>
		
	</ul>
</div>

